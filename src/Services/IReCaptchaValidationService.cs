using System.Threading.Tasks;
using EcoLogic.ReCaptcha.Models;

namespace EcoLogic.ReCaptcha.Services
{
    public interface IReCaptchaValidationService
    {
        /// <summary>
        /// Validates the client captcha token
        /// </summary>
        Task<ReCaptchaResponse> ValidateAsync(string token);
    }
}