using System.Net.Http;

namespace EcoLogic.ReCaptcha.Services
{
    public class ReCaptchaApiClient
    {
        public ReCaptchaApiClient(HttpClient client)
        {
            Client = client;
        }

        public HttpClient Client { get; }
    }
}