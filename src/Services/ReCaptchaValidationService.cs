using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using EcoLogic.ReCaptcha.Extensions;
using EcoLogic.ReCaptcha.Models;
using Microsoft.Extensions.Options;

namespace EcoLogic.ReCaptcha.Services
{
    public class ReCaptchaValidationService : IReCaptchaValidationService
    {
        private readonly ReCaptchaOptions _options;
        private readonly HttpClient _client;

        public ReCaptchaValidationService(ReCaptchaApiClient reCaptchaApiClient, IOptions<ReCaptchaOptions> options)
        {
            _options = options.Value;
            _client = reCaptchaApiClient.Client;
        }

        /// <summary>
        /// Validates the client captcha token
        /// </summary>
        public async Task<ReCaptchaResponse> ValidateAsync(string token)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("secret", _options.Secret),
                new KeyValuePair<string, string>("response", token)
            });
            var response = await _client.PostAsync("/recaptcha/api/siteverify", formContent);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<ReCaptchaResponse>();
        }
    }
}