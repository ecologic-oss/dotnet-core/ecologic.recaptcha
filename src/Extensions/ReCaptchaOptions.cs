namespace EcoLogic.ReCaptcha.Extensions
{
    public class ReCaptchaOptions
    {
        public string Secret { get; set; }

        public bool DisableVerification { get; set; } = false;
    }
}