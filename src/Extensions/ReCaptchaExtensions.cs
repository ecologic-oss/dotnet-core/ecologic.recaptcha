using System;
using EcoLogic.ReCaptcha.Services;
using Microsoft.Extensions.DependencyInjection;

namespace EcoLogic.ReCaptcha.Extensions
{
    public static class ReCaptchaExtensions
    {
        private const string ReCaptchaBaseUrl = "https://www.google.com";

        public static IServiceCollection AddReCaptchaValidationService(this IServiceCollection services, Action<ReCaptchaOptions> options)
        {
            services.Configure(options);
            services.AddHttpClient<ReCaptchaApiClient>(client => { client.BaseAddress = new Uri(ReCaptchaBaseUrl); });
            services.AddTransient<IReCaptchaValidationService, ReCaptchaValidationService>();
            return services;
        }
    }
}