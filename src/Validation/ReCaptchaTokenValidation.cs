using System;
using System.ComponentModel.DataAnnotations;
using EcoLogic.ReCaptcha.Extensions;
using EcoLogic.ReCaptcha.Services;
using Microsoft.Extensions.Options;

namespace EcoLogic.ReCaptcha.Validation
{
    public class ReCaptchaTokenValidation : ValidationAttribute
    {
        private const string DefaultErrorMessage = "Invalid CAPTCHA token.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // resolving services
            var options = (IOptions<ReCaptchaOptions>) validationContext.GetService(typeof(IOptions<ReCaptchaOptions>));
            var reCaptchaApiClient = (ReCaptchaApiClient) validationContext.GetService(typeof(ReCaptchaApiClient));

            if (options == null || reCaptchaApiClient == null)
            {
                throw new ArgumentException("Could not resolve all services");
            }

            if (options.Value.DisableVerification)
            {
                return ValidationResult.Success;
            }

            // validation
            return IsValidCaptchaToken(reCaptchaApiClient, options.Value.Secret, value) ? ValidationResult.Success : new ValidationResult(ErrorMessage ?? DefaultErrorMessage);
        }

        private static bool IsValidCaptchaToken(ReCaptchaApiClient reCaptchaApiClient, string secret, object value)
        {
            return value != null &&
                   value is string tokenValue &&
                   InternalReCaptcheValidationService.Validate(reCaptchaApiClient.Client, secret, tokenValue).Success;
        }
    }
}