using System.Collections.Generic;
using System.Net.Http;
using EcoLogic.ReCaptcha.Models;

namespace EcoLogic.ReCaptcha.Validation
{
    internal static class InternalReCaptcheValidationService
    {
        internal static ReCaptchaResponse Validate(HttpClient client, string secret, string token)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("secret", secret),
                new KeyValuePair<string, string>("response", token)
            });

            var response = client.PostAsync("/recaptcha/api/siteverify", formContent).Result;
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ReCaptchaResponse>().Result;
            return result;
        }
    }
}